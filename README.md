# carbon_benchmarks

This repository contains benchmarks used to test the Carbon algorithm.
The complete description of the benchmarks one can find in [1].
Please cite [1], if you use these benchmarks in your projects.

**[1] Popov, Petr, et al. "Controlled‐advancement rigid‐body optimization of nanosystems." Journal of computational chemistry 40.27 (2019): 2391-2399.**


Please also cite [2], if you use the Docking benchmark in your projects.

[2] Popov, P., & Grudinin, S. (2015).
"Knowledge of native protein–protein interfaces is sufficient to construct predictive models for the selection of binding candidates."
Journal of chemical information and modeling, 55(10), 2242-2255.

# NOTE : The source code of Carbon written in C++ for SAMSON SDK is available at https://gitlab.com/pp_lab/carbon

# Nanosystems benchmark

This benchmark contains the start and final conformations for :
 - trimeric albumin protein complex (PDB ID : 5Z0B), 
 - topoisomerase complex that consists of two protein chains, one DNA fragment, and two chemical compounds (PDB ID : 5GWK), 
 - two artificially designed nanotubes with radii of 4.0A and 5.0A placed one into the other


# Docking benchmark

This benchmark contains the start and final conformations of the putative docking candidates.
Please cite [2], if you use these benchmarks in your projects.

[2] Popov, P., & Grudinin, S. (2015). 
"Knowledge of native protein–protein interfaces is sufficient to construct predictive models for the selection of binding candidates." 
*Journal of chemical information and modeling*, 55(10), 2242-2255.


# Clashes benchmark

This benchmark contains the start and final conformations of three molecular complexes (PDB IDs : 1A4I, 11AS, 1A7N), that we manuallly clashed for the steric overlap.

